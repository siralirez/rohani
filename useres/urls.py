from django.urls import path
from useres.views import *

urlpatterns = [
    path('login', login.as_view()),
    path('register', register.as_view()),
    path('logout', logout.as_view()),

]
